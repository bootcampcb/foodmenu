"use strict";

window.onload = function () {

    document.getElementById("catSelect").onchange = onChangeCat;
}

function onChangeCat() {
    let menu = {
        drinks: [
            "Water", "Tea", "Sweet Tea",
            "Coke", "Dr. Pepper", "Sprite"
        ],
        entrees: [
            "Hamburger w/ Fries",
            "Grilled Cheese w/ Tater Tots",
            "Grilled Chicken w/ Veggies",
            "Chicken Fried Steak w/ Mashed Potatoes",
            "Fried Shrimp w/ Coleslaw",
            "Veggie Plate"
        ],
        desserts: [
            "Cheesecake", "Chocolate Cake", "Snickerdoodle Cookie"
        ]
    };
    const catSelect = document.getElementById("catSelect");
    const selectBox = document.getElementById("selectBox");
    /* Init selection box */
    selectBox.options.length = 0.

    /* Set options based on category */
    let catLength = menu[catSelect.value].length;
    selectBox.size = catLength;
    for (let i = 0; i < catLength; i++) {
        let theOption = new Option(menu[catSelect.value][i], undefined);
        selectBox.appendChild(theOption);
    }
}

function onChangeTeam() {
    const teamSelect = document.getElementById("teamSelect");
    const para = document.getElementById("para");


    return false;
}